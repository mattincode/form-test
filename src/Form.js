import React from "react";

export default class Form extends React.Component {
  constructor(props) {
    super(props);    
    this.renderChildren = this.renderChildren.bind(this);
    this.isFormElement = this.isFormElement.bind(this);
    this.recursiveClone = this.recursiveClone.bind(this);
  }

  renderChildren(childrenElements) {
    if (!childrenElements) {
      return;
    }   
    return childrenElements.map(child => {     
      return this.recursiveClone(child)     
    });    
  }

  recursiveClone(element) {
    var children = [];
    if (element.props && element.props.children.forEach && element.props.children.length > 0) {
      children.push(element.props.children.map(c => {
        return this.recursiveClone(c);
      }));      
    } else if (element.props && element.props.children) {
      children = [this.recursiveClone(element.props.children)]; // Single child
    }
    
    if (this.isFormElement(element)) {
      return React.cloneElement(element, {isCloned: true}, children);  
    } else if (children.length > 0) {
      return React.cloneElement(element, {}, children);  
    } else {
      return element;
    }   
  }  

  isFormElement(element) {
    return element.props !== undefined && element.props.isFormControl
      ? true
      : false;
  }
  
  render() {    
    return <div>{this.renderChildren(this.props.children)}</div>;
  }
}
