import React from 'react';
import Form from './Form';
import FormChild from './FormChild';
import './App.css';

function App() {
  return (
    <div className="App">
      <Form>
        <div key="FirstDiv">sssss</div>
        <FormChild key="FormControl 1" name="first" isFormControl={true}>
          <div>sss</div>
        </FormChild>
        <FormChild key="ReactComponent" name="not a formcontrol">
          <div key="d1">
            <FormChild key="FormControl 2" name="second nested" isFormControl={true}>
              tst2
              <FormChild key="Form Control 2-2" name="third nexted" isFormControl={true}>
                xxx
              </FormChild>
            </FormChild>
          </div>
        </FormChild>
        <FormChild key="ReactComponent 2" isFormControl={true} name="formcontrol with formcontrol">          
            <FormChild key="Nested formControl" name="nested formcontrol" isFormControl={true}>tstXX</FormChild>          
        </FormChild>
      </Form>
    </div>
  );
}

export default App;
