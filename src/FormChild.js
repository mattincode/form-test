import React from "react";
import PropTypes from "prop-types";

class FormChild extends React.Component {
  render() {
    return (
      <div key="name"> 
        {`${this.props.name} : ${this.props.isCloned}`}
        <div>{this.props.children}</div>
      </div>
    );
  }
}

FormChild.propTypes = {
  isFormControl: PropTypes.bool,
  name: PropTypes.string,
  isCloned: PropTypes.bool
};

export default FormChild;
